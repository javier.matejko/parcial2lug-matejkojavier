﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Motor
{
    public class Pasajero
    {
        AccesoDatos.MP_Pasajero mp_Pasajero = new AccesoDatos.MP_Pasajero();
    
        public void Agregar(Entidades.Pasajero pasajero)
        {
            mp_Pasajero.Agregar(pasajero);
        }
        public void Guardar(Entidades.Pasajero pasajero)
        {
            mp_Pasajero.Guardar(pasajero);
        }

        public int Eliminar(Entidades.Pasajero pasajero)
        {
            int resultado = mp_Pasajero.Eliminar(pasajero);
            return resultado;
        }

        public List<Entidades.Pasajero> Buscar()
        {
            List<Entidades.Pasajero> pasajeros = mp_Pasajero.Leer();

            return pasajeros;
        }

        public List<Entidades.Pasajero> Buscar(int id)
        {
            List<Entidades.Pasajero> pasajeros = mp_Pasajero.Leer(id);

            return pasajeros;
        }
        public List<Entidades.Pasajero> Buscar(string nombre)
        {
            List<Entidades.Pasajero> pasajeros = mp_Pasajero.Leer(nombre);

            return pasajeros;
        }
        public List<Entidades.Pasajero> AgregarPasajerosEnVuelo(int id)
        {
            List<Entidades.Pasajero> pasajeros = mp_Pasajero.LeerPasajerosEnVuelo(id);

            return pasajeros;
        }
        public List<Entidades.Pasajero> LeerPasajerosEnVuelo(int id)
        {
            List<Entidades.Pasajero> pasajeros = mp_Pasajero.LeerPasajerosEnVuelo(id);

            return pasajeros;
        }
    }
}
