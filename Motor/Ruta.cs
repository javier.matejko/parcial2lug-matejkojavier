﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motor
{
    public class Ruta
    {
        AccesoDatos.MP_Ruta mp_Ruta = new AccesoDatos.MP_Ruta();

        public List<Entidades.Ruta> Buscar()
        {
            List<Entidades.Ruta> rutas = mp_Ruta.Leer();

            return rutas;
        }
    }
}
