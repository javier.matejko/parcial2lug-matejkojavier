﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motor
{
    public class Vuelo
    {
        AccesoDatos.MP_Vuelo mP_Vuelo = new AccesoDatos.MP_Vuelo();

        public void Agregar(Entidades.Vuelo vuelo)
        {
            mP_Vuelo.Agregar(vuelo);
        }

        public int Cancelar(Entidades.Vuelo vuelo)
        {
            int resultado = mP_Vuelo.Cancelar(vuelo);
            return resultado;
        }

        public List<Entidades.Vuelo> Buscar()
        {
            List<Entidades.Vuelo> vuelos = mP_Vuelo.Leer();

            return vuelos;
        }

        public List<Entidades.Vuelo> Buscar(int id)
        {
            List<Entidades.Vuelo> vuelos = mP_Vuelo.Leer(id);

            return vuelos;
        }
        public List<Entidades.Vuelo> Buscar(int id_destino, int id_tipo, int id_ruta)
        {
            List<Entidades.Vuelo> vuelos = mP_Vuelo.Leer(id_destino, id_tipo, id_ruta);

            return vuelos;
        }
        public List<Entidades.Vuelo> Buscar(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<Entidades.Vuelo> vuelos = mP_Vuelo.Leer(fechaDesde,fechaHasta);

            return vuelos;
        }
        public List<Entidades.Vuelo> Buscar(float porcentaje)
        {
            List<Entidades.Vuelo> vuelos = mP_Vuelo.Leer(porcentaje);

            return vuelos;
        }
        //public List<Entidades.Vuelo> Buscar(string nombre)
        //{
        //    List<Entidades.Vuelo> vuelos = mP_Vuelo.Leer(nombre);

        //    return vuelos;
        //}
    }
}
