﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motor
{
    public class Destino
    {
        AccesoDatos.MP_Destino mp_Destino = new AccesoDatos.MP_Destino();

        public void Agregar(Entidades.Destino destino)
        {
            mp_Destino.Agregar(destino);
        }
        public void Guardar(Entidades.Destino destino)
        {
            mp_Destino.Guardar(destino);
        }

        public int Eliminar(Entidades.Destino destino)
        {
            int resultado = mp_Destino.Eliminar(destino);
            return resultado;
        }

        public List<Entidades.Destino> Buscar()
        {
            List<Entidades.Destino> pasajeros = mp_Destino.Leer();

            return pasajeros;
        }

        public List<Entidades.Destino> Buscar(int id)
        {
            List<Entidades.Destino> pasajeros = mp_Destino.Leer(id);

            return pasajeros;
        }
        public List<Entidades.Destino> Buscar(string nombre)
        {
            List<Entidades.Destino> pasajeros = mp_Destino.Leer(nombre);

            return pasajeros;
        }
    }
}
