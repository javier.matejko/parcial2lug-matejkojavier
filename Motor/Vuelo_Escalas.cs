﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Motor
{
    public class Vuelo_Escalas
    {
        AccesoDatos.MP_Vuelo_Escalas mp_Escalas = new AccesoDatos.MP_Vuelo_Escalas();

        public void Agregar(Entidades.Vuelo_Escalas escala)
        {
            mp_Escalas.AgregarEscalaAVuelo(escala);
        }

        public List<Entidades.Destino> Buscar(int id)
        {
            List<Entidades.Destino> escalas = mp_Escalas.LeerEscalasEnVuelo(id);

            return escalas;
        }
    }
}
