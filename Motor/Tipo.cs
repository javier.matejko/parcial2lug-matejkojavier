﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Motor
{
    public class Tipo
    {
        AccesoDatos.MP_Tipo mp_Tipo = new AccesoDatos.MP_Tipo();

        public List<Entidades.Tipo> Buscar()
        {
            List<Entidades.Tipo> tipos = mp_Tipo.Leer();

            return tipos;
        }
    }
}
