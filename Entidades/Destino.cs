﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace Entidades
{
    public class Destino
    {
        private int id;
        private int id_Ruta;
        private string descripcion;
        private string coordX;
        private string coordY;
        private string coordZ;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Id_Ruta
        {
            get { return id_Ruta; }
            set { id_Ruta = value; }
        }
        public string Descripcion
        {
            get { return descripcion; }
            set { descripcion = value; }
        }
        public string CoordX
        {
            get { return coordX; }
            set { coordX = value; }
        }
        public string CoordY
        {
            get { return coordY; }
            set { coordY = value; }
        }
        public string CoordZ
        {
            get { return coordZ; }
            set { coordZ = value; }
        }
    }
}
