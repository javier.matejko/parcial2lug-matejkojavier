﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Vuelo
    {
        private int id;
        private int id_Destino;
        private int id_Tipo;
        private int id_Ruta;
        private DateTime fechaHora;
        private int capacidad;
        private float porcentajeOcupacion;
        private int cancelado;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        
        public int Id_Destino
        {
            get { return id_Destino; }
            set { id_Destino = value; }
        }
        public int Id_Tipo
        {
            get { return id_Tipo; }
            set { id_Tipo = value; }
        }
        public int Id_Ruta
        {
            get { return id_Ruta; }
            set { id_Ruta = value; }
        }
        public DateTime FechaHora
        {
            get { return fechaHora; }
            set { fechaHora = value; }
        }
        public int Capacidad
        {
            get { return capacidad; }
            set { capacidad = value; }
        }
        public float PorcentajeOcupacion
        {
            get { return porcentajeOcupacion; }
            set { porcentajeOcupacion = value; }
        }
        public int Cancelado
        {
            get { return cancelado; }
            set { cancelado = value; }
        }
    }
}
