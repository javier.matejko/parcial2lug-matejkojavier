﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Vuelo_Pasajeros
    {
        private int id;
        private int id_Vuelo;
        private int id_Pasajero;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        public int Id_Vuelo
        {
            get { return id_Vuelo; }
            set { id_Vuelo = value; }
        }
        public int Id_Pasajero
        {
            get { return id_Pasajero; }
            set { id_Pasajero = value; }
        }
    }
}
