USE [master]
GO
/****** Object:  Database [Parcial2]    Script Date: 17/11/2020 17:33:14 ******/
CREATE DATABASE [Parcial2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Parcial2', FILENAME = N'D:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Parcial2.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Parcial2_log', FILENAME = N'D:\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Parcial2_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Parcial2] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Parcial2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Parcial2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Parcial2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Parcial2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Parcial2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Parcial2] SET ARITHABORT OFF 
GO
ALTER DATABASE [Parcial2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Parcial2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Parcial2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Parcial2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Parcial2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Parcial2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Parcial2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Parcial2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Parcial2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Parcial2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Parcial2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Parcial2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Parcial2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Parcial2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Parcial2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Parcial2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Parcial2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Parcial2] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Parcial2] SET  MULTI_USER 
GO
ALTER DATABASE [Parcial2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Parcial2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Parcial2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Parcial2] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Parcial2] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Parcial2] SET QUERY_STORE = OFF
GO
USE [Parcial2]
GO
/****** Object:  Table [dbo].[Destinos]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Destinos](
	[Id] [int] NOT NULL,
	[Id_Ruta] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[CoordenadaX] [nvarchar](50) NOT NULL,
	[CoordenadaY] [nvarchar](50) NOT NULL,
	[CoordenadaZ] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Destinos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pasajeros]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pasajeros](
	[Id] [int] NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Eliminado] [int] NOT NULL,
 CONSTRAINT [PK_Pasajeros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Ruta]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ruta](
	[Id] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Ruta] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tipos]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tipos](
	[Id] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Tipos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vuelo_Escalas]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vuelo_Escalas](
	[Id] [int] NOT NULL,
	[Id_Vuelo] [int] NOT NULL,
	[Id_Destino] [int] NOT NULL,
 CONSTRAINT [PK_Vuelo_Escalas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vuelo_Pasajeros]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vuelo_Pasajeros](
	[Id] [int] NOT NULL,
	[Id_Vuelo] [int] NOT NULL,
	[Id_Pasajero] [int] NOT NULL,
 CONSTRAINT [PK_Vuelo_Pasajeros] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vuelos]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vuelos](
	[Id] [int] NOT NULL,
	[Id_Destino] [int] NOT NULL,
	[Id_Tipo] [int] NOT NULL,
	[Id_Ruta] [int] NOT NULL,
	[Fecha_Hora] [datetime] NOT NULL,
	[Capacidad] [int] NOT NULL,
	[Porcentaje_Ocupacion] [float] NOT NULL,
	[Cancelado] [int] NOT NULL,
 CONSTRAINT [PK_Vuelos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Destinos] ([Id], [Id_Ruta], [Descripcion], [CoordenadaX], [CoordenadaY], [CoordenadaZ]) VALUES (1, 1, N'Buenos Aires', N'-34.6157', N'-58.5734', N'11')
GO
INSERT [dbo].[Destinos] ([Id], [Id_Ruta], [Descripcion], [CoordenadaX], [CoordenadaY], [CoordenadaZ]) VALUES (2, 2, N'San Pablo(Brasil)', N'-23.6822', N'-46.8755', N'10')
GO
INSERT [dbo].[Destinos] ([Id], [Id_Ruta], [Descripcion], [CoordenadaX], [CoordenadaY], [CoordenadaZ]) VALUES (3, 1, N'Mendoza', N'-32.8833', N'-68.8935', N'13')
GO
INSERT [dbo].[Destinos] ([Id], [Id_Ruta], [Descripcion], [CoordenadaX], [CoordenadaY], [CoordenadaZ]) VALUES (4, 1, N'Salta', N'-24.7961', N'-65.5007', N'12')
GO
INSERT [dbo].[Destinos] ([Id], [Id_Ruta], [Descripcion], [CoordenadaX], [CoordenadaY], [CoordenadaZ]) VALUES (5, 2, N'Roma(Italia)', N'41.91', N'12.3959', N'11')
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (1, N'Juan', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (2, N'Micaela', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (3, N'Ariel', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (4, N'Dario', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (5, N'Mariela', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (6, N'Ezequiel', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (7, N'Tomas', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (8, N'Florencia', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (9, N'Erick', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (10, N'Anibal', 1)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (11, N'Sebastian', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (12, N'Jorge', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (13, N'Camila', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (14, N'Roberto', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (15, N'Samuel', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (16, N'Santiago', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (17, N'Ricardo', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (18, N'Margarita', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (19, N'Manuela', 0)
GO
INSERT [dbo].[Pasajeros] ([Id], [Nombre], [Eliminado]) VALUES (20, N'Esteban', 0)
GO
INSERT [dbo].[Ruta] ([Id], [Descripcion]) VALUES (1, N'Nacional')
GO
INSERT [dbo].[Ruta] ([Id], [Descripcion]) VALUES (2, N'Internacional')
GO
INSERT [dbo].[Tipos] ([Id], [Descripcion]) VALUES (1, N'Arribo')
GO
INSERT [dbo].[Tipos] ([Id], [Descripcion]) VALUES (2, N'Despegue')
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (1, 2, 2)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (2, 2, 1)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (3, 2, 3)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (4, 10, 1)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (5, 10, 3)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (6, 10, 5)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (7, 15, 1)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (8, 15, 4)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (9, 15, 3)
GO
INSERT [dbo].[Vuelo_Escalas] ([Id], [Id_Vuelo], [Id_Destino]) VALUES (10, 16, 3)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (1, 2, 1)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (2, 2, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (3, 2, 3)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (4, 3, 15)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (5, 2, 6)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (6, 3, 16)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (7, 8, 5)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (8, 8, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (9, 8, 13)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (10, 9, 1)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (11, 9, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (12, 9, 3)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (13, 9, 4)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (14, 9, 5)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (15, 9, 6)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (16, 9, 7)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (17, 9, 8)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (18, 9, 9)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (19, 9, 10)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (20, 10, 1)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (21, 10, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (22, 11, 7)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (23, 11, 11)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (24, 11, 12)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (25, 11, 10)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (26, 11, 9)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (27, 11, 16)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (28, 12, 8)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (29, 12, 4)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (30, 12, 11)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (31, 13, 1)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (32, 13, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (33, 13, 3)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (34, 13, 4)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (35, 13, 5)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (36, 13, 6)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (37, 14, 1)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (38, 14, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (39, 14, 3)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (40, 14, 4)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (41, 14, 5)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (42, 14, 6)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (43, 15, 4)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (44, 15, 10)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (45, 15, 1)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (46, 15, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (47, 15, 3)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (48, 15, 5)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (49, 15, 6)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (50, 15, 7)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (51, 15, 8)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (52, 15, 9)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (53, 15, 11)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (54, 15, 12)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (55, 15, 13)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (56, 15, 14)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (57, 15, 15)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (58, 15, 16)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (59, 16, 1)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (60, 16, 2)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (61, 16, 3)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (62, 16, 4)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (63, 16, 5)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (64, 16, 6)
GO
INSERT [dbo].[Vuelo_Pasajeros] ([Id], [Id_Vuelo], [Id_Pasajero]) VALUES (65, 16, 7)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (1, 1, 1, 1, CAST(N'2020-06-23T00:00:00.000' AS DateTime), 200, 100, 1)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (2, 1, 1, 1, CAST(N'2020-06-23T00:00:00.000' AS DateTime), 200, 100, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (3, 1, 1, 1, CAST(N'2020-06-23T00:00:00.000' AS DateTime), 200, 100, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (4, 1, 1, 1, CAST(N'2020-06-23T00:00:00.000' AS DateTime), 200, 100, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (5, 1, 1, 1, CAST(N'2020-06-23T00:00:00.000' AS DateTime), 200, 100, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (6, 1, 1, 1, CAST(N'2020-11-18T09:00:00.000' AS DateTime), 150, 1, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (7, 1, 1, 1, CAST(N'2020-11-17T11:42:51.267' AS DateTime), 111, 1, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (8, 1, 1, 1, CAST(N'2020-11-17T11:47:50.647' AS DateTime), 111, 1, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (9, 2, 1, 2, CAST(N'2020-11-17T11:48:52.813' AS DateTime), 100, 1, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (10, 2, 1, 1, CAST(N'2020-11-17T12:35:12.680' AS DateTime), 555, 1, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (11, 1, 1, 1, CAST(N'2020-11-17T13:30:22.840' AS DateTime), 10, 60, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (12, 1, 1, 1, CAST(N'2020-11-17T13:31:25.367' AS DateTime), 10, 30, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (13, 1, 1, 1, CAST(N'2020-11-17T16:02:07.567' AS DateTime), 5, 120, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (14, 1, 1, 1, CAST(N'2020-11-17T16:02:55.063' AS DateTime), 4, 150, 0)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (15, 2, 2, 2, CAST(N'2020-11-17T16:17:35.293' AS DateTime), 20, 80, 1)
GO
INSERT [dbo].[Vuelos] ([Id], [Id_Destino], [Id_Tipo], [Id_Ruta], [Fecha_Hora], [Capacidad], [Porcentaje_Ocupacion], [Cancelado]) VALUES (16, 1, 1, 1, CAST(N'2020-11-17T17:26:07.557' AS DateTime), 30, 23, 0)
GO
ALTER TABLE [dbo].[Destinos]  WITH CHECK ADD  CONSTRAINT [FK_Destinos_Ruta] FOREIGN KEY([Id_Ruta])
REFERENCES [dbo].[Ruta] ([Id])
GO
ALTER TABLE [dbo].[Destinos] CHECK CONSTRAINT [FK_Destinos_Ruta]
GO
ALTER TABLE [dbo].[Vuelo_Escalas]  WITH CHECK ADD  CONSTRAINT [FK_Vuelo_Escalas_Destinos] FOREIGN KEY([Id_Destino])
REFERENCES [dbo].[Destinos] ([Id])
GO
ALTER TABLE [dbo].[Vuelo_Escalas] CHECK CONSTRAINT [FK_Vuelo_Escalas_Destinos]
GO
ALTER TABLE [dbo].[Vuelo_Escalas]  WITH CHECK ADD  CONSTRAINT [FK_Vuelo_Escalas_Vuelos] FOREIGN KEY([Id_Vuelo])
REFERENCES [dbo].[Vuelos] ([Id])
GO
ALTER TABLE [dbo].[Vuelo_Escalas] CHECK CONSTRAINT [FK_Vuelo_Escalas_Vuelos]
GO
ALTER TABLE [dbo].[Vuelo_Pasajeros]  WITH CHECK ADD  CONSTRAINT [FK_Vuelo_Pasajeros_Pasajeros] FOREIGN KEY([Id_Pasajero])
REFERENCES [dbo].[Pasajeros] ([Id])
GO
ALTER TABLE [dbo].[Vuelo_Pasajeros] CHECK CONSTRAINT [FK_Vuelo_Pasajeros_Pasajeros]
GO
ALTER TABLE [dbo].[Vuelo_Pasajeros]  WITH CHECK ADD  CONSTRAINT [FK_Vuelo_Pasajeros_Vuelos] FOREIGN KEY([Id_Vuelo])
REFERENCES [dbo].[Vuelos] ([Id])
GO
ALTER TABLE [dbo].[Vuelo_Pasajeros] CHECK CONSTRAINT [FK_Vuelo_Pasajeros_Vuelos]
GO
ALTER TABLE [dbo].[Vuelos]  WITH CHECK ADD  CONSTRAINT [FK_Vuelos_Destinos] FOREIGN KEY([Id_Destino])
REFERENCES [dbo].[Destinos] ([Id])
GO
ALTER TABLE [dbo].[Vuelos] CHECK CONSTRAINT [FK_Vuelos_Destinos]
GO
ALTER TABLE [dbo].[Vuelos]  WITH CHECK ADD  CONSTRAINT [FK_Vuelos_Ruta] FOREIGN KEY([Id_Ruta])
REFERENCES [dbo].[Ruta] ([Id])
GO
ALTER TABLE [dbo].[Vuelos] CHECK CONSTRAINT [FK_Vuelos_Ruta]
GO
ALTER TABLE [dbo].[Vuelos]  WITH CHECK ADD  CONSTRAINT [FK_Vuelos_Tipos] FOREIGN KEY([Id_Tipo])
REFERENCES [dbo].[Tipos] ([Id])
GO
ALTER TABLE [dbo].[Vuelos] CHECK CONSTRAINT [FK_Vuelos_Tipos]
GO
/****** Object:  StoredProcedure [dbo].[Destino_Add]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Destino_Add]
@id_ruta int,
@descripcion nvarchar(50),
@coordX float,
@coordY float,
@coordZ float
as
begin

declare @id int
set @id = ISNULL((Select MAX(Id)From Destinos),0)+1

Insert into Destinos(Id, Id_Ruta, Descripcion, CoordenadaX, CoordenadaY, CoordenadaZ) values (@id, @id_ruta, @descripcion, @coordX, @coordY, @coordZ)

end

GO
/****** Object:  StoredProcedure [dbo].[Destino_Eliminar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Destino_Eliminar]
@id int
as
begin

delete from Destinos where id = @id

end

GO
/****** Object:  StoredProcedure [dbo].[Destino_Mod]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Destino_Mod]
@id int,
@id_ruta int,
@descripcion nvarchar(50),
@coordX float,
@coordY float,
@coordZ float
as
begin

Update Destinos set Id_Ruta = @id_ruta, Descripcion = @descripcion, CoordenadaX = @coordX, CoordenadaY = @coordY, CoordenadaZ = @coordZ where id = @id

end

GO
/****** Object:  StoredProcedure [dbo].[Destinos_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Destinos_Listar]
as
begin

select * from Destinos

end

GO
/****** Object:  StoredProcedure [dbo].[Destinos_Listar_Descripcion]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Destinos_Listar_Descripcion]
@descripcion nvarchar(50)
as
begin

select * from Destinos where Descripcion = @descripcion

end

GO
/****** Object:  StoredProcedure [dbo].[Destinos_Listar_Id]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Destinos_Listar_Id]
@id int
as
begin

select * from Destinos where id = @id

end

GO
/****** Object:  StoredProcedure [dbo].[Pasajero_Add]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Pasajero_Add]
@nombre nvarchar(50)
as
begin

declare @id int
set @id = ISNULL((Select MAX(Id)From Pasajeros),0)+1

Insert into Pasajeros(Id, Nombre) values (@id, @nombre)

end

GO
/****** Object:  StoredProcedure [dbo].[Pasajero_Eliminar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pasajero_Eliminar] 
@id int
as
begin

update Pasajeros set eliminado = 1 where id = @id

end

GO
/****** Object:  StoredProcedure [dbo].[Pasajero_Mod]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Pasajero_Mod]
@id int,
@nombre nvarchar(50)
as
begin

update Pasajeros set Nombre = @nombre where id = @id

end

GO
/****** Object:  StoredProcedure [dbo].[Pasajeros_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Pasajeros_Listar]
as
begin

SELECT * FROM pasajeros where eliminado = 0

end

GO
/****** Object:  StoredProcedure [dbo].[Pasajeros_Listar_Id]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[Pasajeros_Listar_Id]
@id int
as
begin

select * from Pasajeros where id = @id and eliminado = 0

end

GO
/****** Object:  StoredProcedure [dbo].[Pasajeros_Listar_Nombre]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Pasajeros_Listar_Nombre]
@nombre nvarchar(50)
as
begin

select * from Pasajeros where Nombre = @nombre and eliminado = 0

end

GO
/****** Object:  StoredProcedure [dbo].[Rutas_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Rutas_Listar]

as
begin

select * from Ruta

end

GO
/****** Object:  StoredProcedure [dbo].[Tipos_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Tipos_Listar]
as
begin

select * from Tipos

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelo_Add]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Vuelo_Add]
@id_destino int,
@id_tipo int,
@id_ruta int,
@fecha_hora datetime,
@capacidad int,
@porcentaje_ocupacion float
as
begin

declare @id int
set @id = ISNULL((Select MAX(Id)From Vuelos),0)+1

insert into Vuelos(Id, Id_Destino, Id_Tipo, Id_Ruta, Fecha_Hora, Capacidad, Porcentaje_Ocupacion, Cancelado) 
			values(@id, @id_destino, @id_tipo, @id_ruta, @fecha_hora, @capacidad, @porcentaje_ocupacion, 0)

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelo_Cancelar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Vuelo_Cancelar]
@id int
as
begin

update Vuelos set Cancelado = 1 where id = @id

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelo_Destino_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Vuelo_Destino_Listar]
@id_vuelo int
as
begin

select * from Vuelo_Escalas where Id_Vuelo = @id_vuelo

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelo_Escala_Add]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[Vuelo_Escala_Add]
@id_destino int
as
begin
--agrega las escalas al vuelo
declare @id int
set @id = ISNULL((Select MAX(Id)From Vuelo_Escalas),0)+1
declare @id_vuelo int
set @id_vuelo = (select MAX(ID) from Vuelos)

insert into Vuelo_Escalas(Id, Id_Vuelo, Id_Destino) 
			values(@id, @id_vuelo, @id_destino)

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelo_Escala_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Vuelo_Escala_Listar]
@id_vuelo int
as
begin
--se listan las escalas del vuelo
select Destinos.Id, Destinos.Id_Ruta, Destinos.Descripcion, Destinos.CoordenadaX, Destinos.CoordenadaY, Destinos.CoordenadaZ from Destinos 
inner join Vuelo_Escalas on Destinos.Id = Vuelo_Escalas.Id_Destino and Vuelo_Escalas.Id_Vuelo = @id_vuelo

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelo_Pasajero_Add]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Vuelo_Pasajero_Add]
@id_pasajero int
as
begin
--agrega los pasajeros al vuelo
declare @id int
set @id = ISNULL((Select MAX(Id)From Vuelo_Pasajeros),0)+1
declare @id_vuelo int
set @id_vuelo = (select MAX(ID) from Vuelos)

insert into Vuelo_Pasajeros(Id, Id_Vuelo, Id_Pasajero) 
			values(@id, @id_vuelo, @id_pasajero)

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelo_Pasajero_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[Vuelo_Pasajero_Listar]
@id_vuelo int
as
begin
select Pasajeros.Id, Pasajeros.Nombre from Pasajeros inner join Vuelo_Pasajeros on Pasajeros.Id = Vuelo_Pasajeros.Id_Pasajero and Vuelo_Pasajeros.Id_Vuelo = @id_vuelo

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelos_Listar]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Vuelos_Listar]
as
begin

select * from Vuelos

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelos_Listar_Fecha]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[Vuelos_Listar_Fecha]
@fecha_desde datetime,
@fecha_hasta datetime
as
begin
--selecciona vuelos por fecha
select * from Vuelos where Fecha_Hora between @fecha_desde and @fecha_hasta

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelos_Listar_Id]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[Vuelos_Listar_Id]
@id int
as
begin

select * from Vuelos where id = @id

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelos_Listar_Porcentaje]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[Vuelos_Listar_Porcentaje]
@porcentaje_ocupado float
as
begin
--agrega las escalas al vuelo
select * from Vuelos where Porcentaje_Ocupacion < @porcentaje_ocupado

end

GO
/****** Object:  StoredProcedure [dbo].[Vuelos_Listar_Tipo]    Script Date: 17/11/2020 17:33:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[Vuelos_Listar_Tipo]
@id_destino int,
@id_tipo int,
@id_ruta int
as
begin
--selecciona vuelos por fecha
select * from Vuelos where Id_Destino = @id_destino and Id_Tipo = @id_tipo and Id_Ruta = @id_ruta

end

GO
USE [master]
GO
ALTER DATABASE [Parcial2] SET  READ_WRITE 
GO
