﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;

namespace AccesoDatos
{
    public class MP_Pasajero
    {
        private Acceso acceso = new Acceso();
        public void Agregar(Entidades.Pasajero pasajero)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre", pasajero.Nombre));

            acceso.Escribir("Pasajero_Add", parametros);

            acceso.CerrarConexion();
        }
        public void Guardar(Entidades.Pasajero pasajero)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", pasajero.Id));
            parametros.Add(acceso.CrearParametro("@nombre", pasajero.Nombre));

            acceso.Escribir("Pasajero_Mod", parametros);

            acceso.CerrarConexion();
        }

        public List<Entidades.Pasajero> Leer()
        {
            DataTable tabla = new DataTable();
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();
            acceso.AbrirConexion();

            tabla = acceso.Leer("Pasajeros_Listar");

            acceso.CerrarConexion();

            foreach(DataRow registro in tabla.Rows)
            {
                Entidades.Pasajero p = new Entidades.Pasajero();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                pasajeros.Add(p);
            }

            return pasajeros;
        }
        public List<Entidades.Pasajero> Leer(int id)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", id));

            tabla = acceso.Leer("Pasajeros_Listar_Id", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Pasajero p = new Entidades.Pasajero();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                pasajeros.Add(p);
            }

            return pasajeros;
        }
        public List<Entidades.Pasajero> Leer(string nombre)
        {
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@nombre", nombre));

            DataTable tabla = acceso.Leer("Pasajeros_Listar_Nombre", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Pasajero p = new Entidades.Pasajero();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                pasajeros.Add(p);
            }

            return pasajeros;
        }
        public List<Entidades.Pasajero> LeerPasajerosVuelo(int id_vuelo)
        {
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_vuelo", id_vuelo));

            DataTable tabla = acceso.Leer("Vuelo_Pasajero_Listar", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Pasajero p = new Entidades.Pasajero();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                pasajeros.Add(p);
            }

            return pasajeros;
        }
        public int Eliminar(Entidades.Pasajero pasajero)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", pasajero.Id));

            int resultado = acceso.Escribir("Pasajero_Eliminar", parametros);

            acceso.CerrarConexion();

            return resultado;
        }

        public List<Entidades.Pasajero> LeerPasajerosEnVuelo(int id)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_vuelo", id));

            tabla = acceso.Leer("Vuelo_Pasajero_Listar", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Pasajero p = new Entidades.Pasajero();
                p.Id = int.Parse(registro[0].ToString());
                p.Nombre = registro[1].ToString();
                pasajeros.Add(p);
            }

            return pasajeros;
        }

        
    }
}
