﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class Acceso
    {
        private SqlConnection conexion;

        public void AbrirConexion()
        {
            conexion = new SqlConnection("Data Source=.\\Sqlexpress; Initial Catalog=Parcial2; Integrated Security=SSPI");
            conexion.Open();
        }
        public void CerrarConexion()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CrearComando(string sqlString, List<IDbDataParameter> parametros = null, CommandType tipo = CommandType.StoredProcedure)
        {
            SqlCommand comando = new SqlCommand(sqlString, conexion);
            comando.CommandType = tipo;

            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }

            return comando;
        }
        public int Escribir(string sqlString, List<IDbDataParameter> parametros = null)
        {
            SqlCommand comando = CrearComando(sqlString, parametros);
            int resultado;

            try
            {
                resultado = comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                resultado = -1;
            }

            return resultado;
        }

        public DataTable Leer(string sqlString, List<IDbDataParameter> parametros = null)
        {
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            sqlDataAdapter.SelectCommand = CrearComando(sqlString, parametros);
            DataTable tabla = new DataTable();
            sqlDataAdapter.Fill(tabla);
            return tabla;
        }

        public IDbDataParameter CrearParametro(string nombre, int valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor);
            parameter.DbType = DbType.Int32;
            return parameter;
        }
        public IDbDataParameter CrearParametro(string nombre, float valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor);
            parameter.DbType = DbType.Single;
            return parameter;
        }
        public IDbDataParameter CrearParametro(string nombre, Int64 valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor);
            parameter.DbType = DbType.Int64;
            return parameter;
        }

        public IDbDataParameter CrearParametro(string nombre, string valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor);
            parameter.DbType = DbType.String;
            return parameter;
        }

        public IDbDataParameter CrearParametro(string nombre, DateTime valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor);
            parameter.DbType = DbType.DateTime;
            return parameter;
        }
        public IDbDataParameter CrearParametro(string nombre, decimal valor)
        {
            SqlParameter parameter = new SqlParameter(nombre, valor);
            parameter.DbType = DbType.Decimal;
            return parameter;
        }
    }
}
