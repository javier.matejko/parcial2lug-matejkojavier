﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class MP_Vuelo_Pasajeros
    {
        private Acceso acceso = new Acceso();
        public void AgregarPasajeroAVuelo(Entidades.Vuelo_Pasajeros pasajero)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_pasajero", pasajero.Id_Pasajero));

            acceso.Escribir("Vuelo_Pasajero_Add", parametros);

            acceso.CerrarConexion();
        }
    }
}
