﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class MP_Vuelo_Escalas
    {
        private Acceso acceso = new Acceso();
        public void AgregarEscalaAVuelo(Entidades.Vuelo_Escalas escala)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_destino", escala.Id_Destino));

            acceso.Escribir("Vuelo_Escala_Add", parametros);

            acceso.CerrarConexion();
        }

        public List<Entidades.Destino> LeerEscalasEnVuelo(int id)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Destino> escalas = new List<Entidades.Destino>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_vuelo", id));

            tabla = acceso.Leer("Vuelo_Escala_Listar", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Destino d = new Entidades.Destino();
                d.Id = int.Parse(registro[0].ToString());
                d.Id_Ruta = int.Parse(registro[1].ToString());
                d.Descripcion = registro[2].ToString();
                d.CoordX = registro[3].ToString();
                d.CoordY = registro[4].ToString();
                d.CoordZ = registro[5].ToString();
                escalas.Add(d);
            }

            return escalas;
        }
    }
}
