﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Entidades;

namespace AccesoDatos
{
    public class MP_Vuelo
    {
        private Acceso acceso = new Acceso();
        private MP_Destino mp_Destino = new MP_Destino();
        private MP_Pasajero mp_Pasajero = new MP_Pasajero();
        public void Agregar(Entidades.Vuelo vuelo)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_destino", vuelo.Id_Destino));
            parametros.Add(acceso.CrearParametro("@id_tipo", vuelo.Id_Tipo));
            parametros.Add(acceso.CrearParametro("@id_ruta", vuelo.Id_Ruta));
            parametros.Add(acceso.CrearParametro("@fecha_hora", vuelo.FechaHora));
            parametros.Add(acceso.CrearParametro("@capacidad", vuelo.Capacidad));
            parametros.Add(acceso.CrearParametro("@porcentaje_ocupacion", vuelo.PorcentajeOcupacion));

            acceso.Escribir("Vuelo_Add", parametros);

            acceso.CerrarConexion();
        }
        public int Cancelar(Entidades.Vuelo vuelo)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", vuelo.Id));

            int resultado = acceso.Escribir("Vuelo_Cancelar", parametros);

            acceso.CerrarConexion();

            return resultado;
        }

        public List<Entidades.Vuelo> Leer()
        {
            DataTable tabla = new DataTable();
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();
            acceso.AbrirConexion();

            tabla = acceso.Leer("Vuelos_Listar");

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Vuelo v = new Entidades.Vuelo();
                v.Id = int.Parse(registro[0].ToString());
                v.Id_Destino = int.Parse(registro[1].ToString());
                v.Id_Tipo = int.Parse(registro[2].ToString());
                v.Id_Ruta = int.Parse(registro[3].ToString());
                v.FechaHora = DateTime.Parse(registro[4].ToString());
                v.Capacidad = int.Parse(registro[5].ToString());
                v.PorcentajeOcupacion = float.Parse(registro[6].ToString());
                v.Cancelado = int.Parse(registro[7].ToString());
                vuelos.Add(v);
            }

            return vuelos;
        }
        public List<Entidades.Vuelo> Leer(int id)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", id));

            tabla = acceso.Leer("Vuelos_Listar_Id", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Vuelo v = new Entidades.Vuelo();
                v.Id = int.Parse(registro[0].ToString());
                v.Id_Destino = int.Parse(registro[1].ToString());
                v.Id_Tipo = int.Parse(registro[2].ToString());
                v.Id_Ruta = int.Parse(registro[3].ToString());
                v.FechaHora = DateTime.Parse(registro[4].ToString());
                v.Capacidad = int.Parse(registro[5].ToString());
                v.PorcentajeOcupacion = float.Parse(registro[6].ToString());
                v.Cancelado = int.Parse(registro[7].ToString());
                vuelos.Add(v);
            }

            return vuelos;
        }
        public List<Entidades.Vuelo> Leer(float porcentajeOcupado)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@porcentaje_ocupado", porcentajeOcupado));

            tabla = acceso.Leer("Vuelos_Listar_Porcentaje", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Vuelo v = new Entidades.Vuelo();
                v.Id = int.Parse(registro[0].ToString());
                v.Id_Destino = int.Parse(registro[1].ToString());
                v.Id_Tipo = int.Parse(registro[2].ToString());
                v.Id_Ruta = int.Parse(registro[3].ToString());
                v.FechaHora = DateTime.Parse(registro[4].ToString());
                v.Capacidad = int.Parse(registro[5].ToString());
                v.PorcentajeOcupacion = float.Parse(registro[6].ToString());
                v.Cancelado = int.Parse(registro[7].ToString());
                vuelos.Add(v);
            }

            return vuelos;
        }
        public List<Entidades.Vuelo> Leer(int id_destino, int id_tipo, int id_ruta)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_destino", id_destino));
            parametros.Add(acceso.CrearParametro("@id_tipo", id_tipo));
            parametros.Add(acceso.CrearParametro("@id_ruta", id_ruta));

            tabla = acceso.Leer("Vuelos_Listar_Tipo", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Vuelo v = new Entidades.Vuelo();
                v.Id = int.Parse(registro[0].ToString());
                v.Id_Destino = int.Parse(registro[1].ToString());
                v.Id_Tipo = int.Parse(registro[2].ToString());
                v.Id_Ruta = int.Parse(registro[3].ToString());
                v.FechaHora = DateTime.Parse(registro[4].ToString());
                v.Capacidad = int.Parse(registro[5].ToString());
                v.PorcentajeOcupacion = float.Parse(registro[6].ToString());
                v.Cancelado = int.Parse(registro[7].ToString());
                vuelos.Add(v);
            }

            return vuelos;
        }

        public List<Entidades.Vuelo> Leer(DateTime fechaDesde, DateTime fechaHasta)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@fecha_desde", fechaDesde));
            parametros.Add(acceso.CrearParametro("@fecha_hasta", fechaHasta));

            tabla = acceso.Leer("Vuelos_Listar_Fecha", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Vuelo v = new Entidades.Vuelo();
                v.Id = int.Parse(registro[0].ToString());
                v.Id_Destino = int.Parse(registro[1].ToString());
                v.Id_Tipo = int.Parse(registro[2].ToString());
                v.Id_Ruta = int.Parse(registro[3].ToString());
                v.FechaHora = DateTime.Parse(registro[4].ToString());
                v.Capacidad = int.Parse(registro[5].ToString());
                v.PorcentajeOcupacion = float.Parse(registro[6].ToString());
                v.Cancelado = int.Parse(registro[7].ToString());
                vuelos.Add(v);
            }

            return vuelos;
        }
        //public List<Entidades.Destino> Leer(string nombre)
        //{
        //    List<Entidades.Destino> destinos = new List<Entidades.Destino>();

        //    acceso.AbrirConexion();

        //    List<IDbDataParameter> parametros = new List<IDbDataParameter>();
        //    parametros.Add(acceso.CrearParametro("@descripcion", nombre));

        //    DataTable tabla = acceso.Leer("Destinos_Listar_Descripcion", parametros);

        //    acceso.CerrarConexion();

        //    foreach (DataRow registro in tabla.Rows)
        //    {
        //        Entidades.Destino d = new Entidades.Destino();
        //        d.Id = int.Parse(registro[0].ToString());
        //        d.Id_Ruta = int.Parse(registro[1].ToString());
        //        d.Descripcion = registro[2].ToString();
        //        d.CoordX = float.Parse(registro[3].ToString());
        //        d.CoordY = float.Parse(registro[4].ToString());
        //        d.CoordZ = float.Parse(registro[5].ToString());
        //        destinos.Add(d);
        //    }

        //    return destinos;
        //}
        public int Eliminar(Entidades.Destino destino)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", destino.Id));

            int resultado = acceso.Escribir("Destino_Eliminar", parametros);

            acceso.CerrarConexion();

            return resultado;
        }
    }
}
