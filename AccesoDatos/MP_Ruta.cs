﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class MP_Ruta
    {
        private Acceso acceso = new Acceso();

        public List<Entidades.Ruta> Leer()
        {
            DataTable tabla = new DataTable();
            List<Entidades.Ruta> rutas = new List<Entidades.Ruta>();
            acceso.AbrirConexion();

            tabla = acceso.Leer("Rutas_Listar");

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Ruta r = new Entidades.Ruta();
                r.Id = int.Parse(registro[0].ToString());
                r.Descripcion = registro[1].ToString();
                rutas.Add(r);
            }

            return rutas;
        }
    }
}
