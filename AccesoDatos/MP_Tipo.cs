﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class MP_Tipo
    {
        private Acceso acceso = new Acceso();

        public List<Entidades.Tipo> Leer()
        {
            DataTable tabla = new DataTable();
            List<Entidades.Tipo> tipos = new List<Entidades.Tipo>();
            acceso.AbrirConexion();

            tabla = acceso.Leer("Tipos_Listar");

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Tipo t = new Entidades.Tipo();
                t.Id = int.Parse(registro[0].ToString());
                t.Descripcion = registro[1].ToString();
                tipos.Add(t);
            }

            return tipos;
        }
    }
}
