﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace AccesoDatos
{
    public class MP_Destino
    {
        private Acceso acceso = new Acceso();
        public void Agregar(Entidades.Destino destino)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id_ruta", destino.Id_Ruta));
            parametros.Add(acceso.CrearParametro("@descripcion", destino.Descripcion));
            parametros.Add(acceso.CrearParametro("@coordX", destino.CoordX));
            parametros.Add(acceso.CrearParametro("@coordY", destino.CoordY));
            parametros.Add(acceso.CrearParametro("@coordZ", destino.CoordZ));

            acceso.Escribir("Destino_Add", parametros);

            acceso.CerrarConexion();
        }
        public void Guardar(Entidades.Destino destino)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", destino.Id));
            parametros.Add(acceso.CrearParametro("@id_ruta", destino.Id_Ruta));
            parametros.Add(acceso.CrearParametro("@descripcion", destino.Descripcion));
            parametros.Add(acceso.CrearParametro("@coordx", destino.CoordX));
            parametros.Add(acceso.CrearParametro("@coordY", destino.CoordY));
            parametros.Add(acceso.CrearParametro("@coordZ", destino.CoordZ));

            acceso.Escribir("Destino_Mod", parametros);

            acceso.CerrarConexion();
        }

        public List<Entidades.Destino> Leer()
        {
            DataTable tabla = new DataTable();
            List<Entidades.Destino> destinos = new List<Entidades.Destino>();
            acceso.AbrirConexion();

            tabla = acceso.Leer("Destinos_Listar");

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Destino d = new Entidades.Destino();
                d.Id = int.Parse(registro[0].ToString());
                d.Id_Ruta = int.Parse(registro[1].ToString());
                d.Descripcion = registro[2].ToString();
                d.CoordX = registro[3].ToString();
                d.CoordY = registro[4].ToString();
                d.CoordZ = registro[5].ToString();
                destinos.Add(d);
            }

            return destinos;
        }
        public List<Entidades.Destino> Leer(int id)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Destino> destinos = new List<Entidades.Destino>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", id));

            tabla = acceso.Leer("Destinos_Listar_Id", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Destino d = new Entidades.Destino();
                d.Id = int.Parse(registro[0].ToString());
                d.Id_Ruta = int.Parse(registro[1].ToString());
                d.Descripcion = registro[2].ToString();
                d.CoordX = registro[3].ToString();
                d.CoordY = registro[4].ToString();
                d.CoordZ = registro[5].ToString();
                destinos.Add(d);
            }

            return destinos;
        }
        public List<Entidades.Destino> Leer(string nombre)
        {
            List<Entidades.Destino> destinos = new List<Entidades.Destino>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@descripcion", nombre));

            DataTable tabla = acceso.Leer("Destinos_Listar_Descripcion", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Destino d = new Entidades.Destino();
                d.Id = int.Parse(registro[0].ToString());
                d.Id_Ruta = int.Parse(registro[1].ToString());
                d.Descripcion = registro[2].ToString();
                d.CoordX = registro[3].ToString();
                d.CoordY = registro[4].ToString();
                d.CoordZ = registro[5].ToString();
                destinos.Add(d);
            }

            return destinos;
        }
        public List<Entidades.Destino> LeerEscalas(int id)
        {
            DataTable tabla = new DataTable();
            List<Entidades.Destino> destinos = new List<Entidades.Destino>();

            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@id", id));

            tabla = acceso.Leer("Vuelo_Destino_Listar", parametros);

            acceso.CerrarConexion();

            foreach (DataRow registro in tabla.Rows)
            {
                Entidades.Destino d = new Entidades.Destino();
                d.Id = int.Parse(registro[0].ToString());
                d.Id_Ruta = int.Parse(registro[1].ToString());
                d.Descripcion = registro[2].ToString();
                d.CoordX = registro[3].ToString();
                d.CoordY = registro[4].ToString();
                d.CoordZ = registro[5].ToString();
                destinos.Add(d);
            }

            return destinos;
        }
        public int Eliminar(Entidades.Destino destino)
        {
            acceso.AbrirConexion();

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();

            parametros.Add(acceso.CrearParametro("@id", destino.Id));

            int resultado = acceso.Escribir("Destino_Eliminar", parametros);

            acceso.CerrarConexion();

            return resultado;
        }
    }
}
