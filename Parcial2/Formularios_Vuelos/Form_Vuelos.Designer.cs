﻿namespace Parcial2.Formularios_Vuelos
{
    partial class Form_Vuelos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnBuscarTodos = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.grdDatos = new System.Windows.Forms.DataGridView();
            this.dtpFecha_Desde = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.chkTodos = new System.Windows.Forms.CheckBox();
            this.dtpFecha_Hasta = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cboxRuta = new System.Windows.Forms.ComboBox();
            this.cboxTipo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.chkPorcentaje = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cboxDestino = new System.Windows.Forms.ComboBox();
            this.grdDatosAdicionales = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.grdDatosAdicionalesEscalas = new System.Windows.Forms.DataGridView();
            this.btnBuscar_Fecha = new System.Windows.Forms.Button();
            this.btnBuscar_Tipo = new System.Windows.Forms.Button();
            this.btnVerMapa = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grdDatos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDatosAdicionales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDatosAdicionalesEscalas)).BeginInit();
            this.SuspendLayout();
            // 
            // btnBuscarTodos
            // 
            this.btnBuscarTodos.Location = new System.Drawing.Point(579, 2);
            this.btnBuscarTodos.Name = "btnBuscarTodos";
            this.btnBuscarTodos.Size = new System.Drawing.Size(115, 23);
            this.btnBuscarTodos.TabIndex = 23;
            this.btnBuscarTodos.Text = "Buscar todos";
            this.btnBuscarTodos.UseVisualStyleBackColor = true;
            this.btnBuscarTodos.Click += new System.EventHandler(this.btnBuscarTodos_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Id:";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(33, 4);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(84, 20);
            this.txtId.TabIndex = 19;
            this.txtId.TextChanged += new System.EventHandler(this.txtId_TextChanged);
            // 
            // grdDatos
            // 
            this.grdDatos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.grdDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDatos.Location = new System.Drawing.Point(3, 149);
            this.grdDatos.Name = "grdDatos";
            this.grdDatos.Size = new System.Drawing.Size(573, 319);
            this.grdDatos.TabIndex = 18;
            this.grdDatos.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdDatos_CellClick);
            // 
            // dtpFecha_Desde
            // 
            this.dtpFecha_Desde.Location = new System.Drawing.Point(86, 39);
            this.dtpFecha_Desde.Name = "dtpFecha_Desde";
            this.dtpFecha_Desde.Size = new System.Drawing.Size(200, 20);
            this.dtpFecha_Desde.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Fecha desde:";
            // 
            // chkTodos
            // 
            this.chkTodos.AutoSize = true;
            this.chkTodos.Checked = true;
            this.chkTodos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTodos.Location = new System.Drawing.Point(132, 7);
            this.chkTodos.Name = "chkTodos";
            this.chkTodos.Size = new System.Drawing.Size(56, 17);
            this.chkTodos.TabIndex = 27;
            this.chkTodos.Text = "Todos";
            this.chkTodos.UseVisualStyleBackColor = true;
            this.chkTodos.CheckedChanged += new System.EventHandler(this.chkTodos_CheckedChanged);
            // 
            // dtpFecha_Hasta
            // 
            this.dtpFecha_Hasta.Location = new System.Drawing.Point(370, 39);
            this.dtpFecha_Hasta.Name = "dtpFecha_Hasta";
            this.dtpFecha_Hasta.Size = new System.Drawing.Size(200, 20);
            this.dtpFecha_Hasta.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(292, 44);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Fecha hasta:";
            // 
            // cboxRuta
            // 
            this.cboxRuta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxRuta.FormattingEnabled = true;
            this.cboxRuta.Location = new System.Drawing.Point(59, 70);
            this.cboxRuta.Name = "cboxRuta";
            this.cboxRuta.Size = new System.Drawing.Size(168, 21);
            this.cboxRuta.TabIndex = 30;
            // 
            // cboxTipo
            // 
            this.cboxTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxTipo.FormattingEnabled = true;
            this.cboxTipo.Location = new System.Drawing.Point(279, 70);
            this.cboxTipo.Name = "cboxTipo";
            this.cboxTipo.Size = new System.Drawing.Size(168, 21);
            this.cboxTipo.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 75);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Ruta:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(242, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Tipo:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(725, 36);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(115, 23);
            this.btnCancelar.TabIndex = 34;
            this.btnCancelar.Text = "Cancelar vuelo";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // chkPorcentaje
            // 
            this.chkPorcentaje.AutoSize = true;
            this.chkPorcentaje.Location = new System.Drawing.Point(194, 7);
            this.chkPorcentaje.Name = "chkPorcentaje";
            this.chkPorcentaje.Size = new System.Drawing.Size(46, 17);
            this.chkPorcentaje.TabIndex = 37;
            this.chkPorcentaje.Text = "40%";
            this.chkPorcentaje.UseVisualStyleBackColor = true;
            this.chkPorcentaje.CheckedChanged += new System.EventHandler(this.chkPorcentaje_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 102);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 42;
            this.label9.Text = "Destino:";
            // 
            // cboxDestino
            // 
            this.cboxDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxDestino.FormattingEnabled = true;
            this.cboxDestino.Location = new System.Drawing.Point(59, 97);
            this.cboxDestino.Name = "cboxDestino";
            this.cboxDestino.Size = new System.Drawing.Size(168, 21);
            this.cboxDestino.TabIndex = 41;
            // 
            // grdDatosAdicionales
            // 
            this.grdDatosAdicionales.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdDatosAdicionales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDatosAdicionales.Location = new System.Drawing.Point(582, 149);
            this.grdDatosAdicionales.Name = "grdDatosAdicionales";
            this.grdDatosAdicionales.Size = new System.Drawing.Size(258, 150);
            this.grdDatosAdicionales.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(579, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "PASAJEROS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 45;
            this.label7.Text = "VUELOS";
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(725, 2);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(115, 23);
            this.btnAgregar.TabIndex = 46;
            this.btnAgregar.Text = "Agregar vuelo";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // grdDatosAdicionalesEscalas
            // 
            this.grdDatosAdicionalesEscalas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdDatosAdicionalesEscalas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdDatosAdicionalesEscalas.Location = new System.Drawing.Point(582, 326);
            this.grdDatosAdicionalesEscalas.Name = "grdDatosAdicionalesEscalas";
            this.grdDatosAdicionalesEscalas.Size = new System.Drawing.Size(258, 142);
            this.grdDatosAdicionalesEscalas.TabIndex = 47;
            // 
            // btnBuscar_Fecha
            // 
            this.btnBuscar_Fecha.Location = new System.Drawing.Point(579, 36);
            this.btnBuscar_Fecha.Name = "btnBuscar_Fecha";
            this.btnBuscar_Fecha.Size = new System.Drawing.Size(115, 23);
            this.btnBuscar_Fecha.TabIndex = 48;
            this.btnBuscar_Fecha.Text = "Buscar por fecha";
            this.btnBuscar_Fecha.UseVisualStyleBackColor = true;
            this.btnBuscar_Fecha.Click += new System.EventHandler(this.btnBuscar_Fecha_Click);
            // 
            // btnBuscar_Tipo
            // 
            this.btnBuscar_Tipo.Location = new System.Drawing.Point(579, 68);
            this.btnBuscar_Tipo.Name = "btnBuscar_Tipo";
            this.btnBuscar_Tipo.Size = new System.Drawing.Size(115, 23);
            this.btnBuscar_Tipo.TabIndex = 49;
            this.btnBuscar_Tipo.Text = "Buscar por tipo";
            this.btnBuscar_Tipo.UseVisualStyleBackColor = true;
            this.btnBuscar_Tipo.Click += new System.EventHandler(this.btnBuscar_Tipo_Click);
            // 
            // btnVerMapa
            // 
            this.btnVerMapa.Location = new System.Drawing.Point(725, 68);
            this.btnVerMapa.Name = "btnVerMapa";
            this.btnVerMapa.Size = new System.Drawing.Size(115, 23);
            this.btnVerMapa.TabIndex = 50;
            this.btnVerMapa.Text = "Ver mapa";
            this.btnVerMapa.UseVisualStyleBackColor = true;
            this.btnVerMapa.Click += new System.EventHandler(this.btnVerMapa_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(582, 310);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 51;
            this.label8.Text = "ESCALA/S";
            // 
            // Form_Vuelos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(848, 469);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnVerMapa);
            this.Controls.Add(this.btnBuscar_Tipo);
            this.Controls.Add(this.btnBuscar_Fecha);
            this.Controls.Add(this.grdDatosAdicionalesEscalas);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.grdDatosAdicionales);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cboxDestino);
            this.Controls.Add(this.chkPorcentaje);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cboxTipo);
            this.Controls.Add(this.cboxRuta);
            this.Controls.Add(this.dtpFecha_Hasta);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chkTodos);
            this.Controls.Add(this.dtpFecha_Desde);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBuscarTodos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.grdDatos);
            this.Name = "Form_Vuelos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Vuelos";
            this.Load += new System.EventHandler(this.Form_Vuelos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdDatos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDatosAdicionales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdDatosAdicionalesEscalas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnBuscarTodos;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.DataGridView grdDatos;
        private System.Windows.Forms.DateTimePicker dtpFecha_Desde;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chkTodos;
        private System.Windows.Forms.DateTimePicker dtpFecha_Hasta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboxRuta;
        private System.Windows.Forms.ComboBox cboxTipo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.CheckBox chkPorcentaje;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboxDestino;
        private System.Windows.Forms.DataGridView grdDatosAdicionales;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.DataGridView grdDatosAdicionalesEscalas;
        private System.Windows.Forms.Button btnBuscar_Fecha;
        private System.Windows.Forms.Button btnBuscar_Tipo;
        private System.Windows.Forms.Button btnVerMapa;
        private System.Windows.Forms.Label label8;
    }
}