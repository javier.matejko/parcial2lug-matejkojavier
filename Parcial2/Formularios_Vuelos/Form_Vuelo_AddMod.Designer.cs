﻿namespace Parcial2.Formularios_Vuelos
{
    partial class Form_Vuelo_AddMod
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboxRuta = new System.Windows.Forms.ComboBox();
            this.cboxDestino = new System.Windows.Forms.ComboBox();
            this.cboxTipo = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpFecha_Hora = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCapacidad = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cboxPasajeros = new System.Windows.Forms.ComboBox();
            this.grdPasajeros = new System.Windows.Forms.DataGridView();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAgregarEscala = new System.Windows.Forms.Button();
            this.grdEscalas = new System.Windows.Forms.DataGridView();
            this.cboxEscalas = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.grdPasajeros)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdEscalas)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "Ruta:";
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(441, 356);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 22;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(279, 356);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(75, 23);
            this.btnGuardar.TabIndex = 20;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 18;
            this.label1.Text = "Destino:";
            // 
            // cboxRuta
            // 
            this.cboxRuta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxRuta.FormattingEnabled = true;
            this.cboxRuta.Location = new System.Drawing.Point(93, 67);
            this.cboxRuta.Name = "cboxRuta";
            this.cboxRuta.Size = new System.Drawing.Size(298, 21);
            this.cboxRuta.TabIndex = 30;
            // 
            // cboxDestino
            // 
            this.cboxDestino.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxDestino.FormattingEnabled = true;
            this.cboxDestino.Location = new System.Drawing.Point(93, 12);
            this.cboxDestino.Name = "cboxDestino";
            this.cboxDestino.Size = new System.Drawing.Size(298, 21);
            this.cboxDestino.TabIndex = 31;
            // 
            // cboxTipo
            // 
            this.cboxTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxTipo.FormattingEnabled = true;
            this.cboxTipo.Location = new System.Drawing.Point(93, 40);
            this.cboxTipo.Name = "cboxTipo";
            this.cboxTipo.Size = new System.Drawing.Size(298, 21);
            this.cboxTipo.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Tipo:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(406, 15);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Fecha y Hora:";
            // 
            // dtpFecha_Hora
            // 
            this.dtpFecha_Hora.CustomFormat = "\"hh:mm:ss tt dd/MM/yyyy\"";
            this.dtpFecha_Hora.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFecha_Hora.Location = new System.Drawing.Point(489, 9);
            this.dtpFecha_Hora.Name = "dtpFecha_Hora";
            this.dtpFecha_Hora.Size = new System.Drawing.Size(298, 20);
            this.dtpFecha_Hora.TabIndex = 35;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(406, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 13);
            this.label5.TabIndex = 36;
            this.label5.Text = "Capacidad:";
            // 
            // txtCapacidad
            // 
            this.txtCapacidad.Location = new System.Drawing.Point(489, 36);
            this.txtCapacidad.Name = "txtCapacidad";
            this.txtCapacidad.Size = new System.Drawing.Size(298, 20);
            this.txtCapacidad.TabIndex = 37;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 109);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Pasajeros:";
            // 
            // cboxPasajeros
            // 
            this.cboxPasajeros.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxPasajeros.FormattingEnabled = true;
            this.cboxPasajeros.Location = new System.Drawing.Point(93, 106);
            this.cboxPasajeros.Name = "cboxPasajeros";
            this.cboxPasajeros.Size = new System.Drawing.Size(217, 21);
            this.cboxPasajeros.TabIndex = 39;
            // 
            // grdPasajeros
            // 
            this.grdPasajeros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPasajeros.Location = new System.Drawing.Point(12, 133);
            this.grdPasajeros.Name = "grdPasajeros";
            this.grdPasajeros.Size = new System.Drawing.Size(379, 196);
            this.grdPasajeros.TabIndex = 40;
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(316, 105);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 41;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(406, 110);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(47, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Escalas:";
            // 
            // btnAgregarEscala
            // 
            this.btnAgregarEscala.Location = new System.Drawing.Point(713, 105);
            this.btnAgregarEscala.Name = "btnAgregarEscala";
            this.btnAgregarEscala.Size = new System.Drawing.Size(75, 23);
            this.btnAgregarEscala.TabIndex = 45;
            this.btnAgregarEscala.Text = "Agregar";
            this.btnAgregarEscala.UseVisualStyleBackColor = true;
            this.btnAgregarEscala.Click += new System.EventHandler(this.btnAgregarEscala_Click);
            // 
            // grdEscalas
            // 
            this.grdEscalas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdEscalas.Location = new System.Drawing.Point(409, 133);
            this.grdEscalas.Name = "grdEscalas";
            this.grdEscalas.Size = new System.Drawing.Size(379, 196);
            this.grdEscalas.TabIndex = 44;
            // 
            // cboxEscalas
            // 
            this.cboxEscalas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboxEscalas.FormattingEnabled = true;
            this.cboxEscalas.Location = new System.Drawing.Point(490, 106);
            this.cboxEscalas.Name = "cboxEscalas";
            this.cboxEscalas.Size = new System.Drawing.Size(217, 21);
            this.cboxEscalas.TabIndex = 43;
            // 
            // Form_Vuelo_AddMod
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 390);
            this.Controls.Add(this.btnAgregarEscala);
            this.Controls.Add(this.grdEscalas);
            this.Controls.Add(this.cboxEscalas);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnAgregar);
            this.Controls.Add(this.grdPasajeros);
            this.Controls.Add(this.cboxPasajeros);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtCapacidad);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dtpFecha_Hora);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cboxTipo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cboxDestino);
            this.Controls.Add(this.cboxRuta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.label1);
            this.Name = "Form_Vuelo_AddMod";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vuelo Agregar";
            this.Load += new System.EventHandler(this.Form_Vuelo_AddMod_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grdPasajeros)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdEscalas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboxRuta;
        private System.Windows.Forms.ComboBox cboxDestino;
        private System.Windows.Forms.ComboBox cboxTipo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpFecha_Hora;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCapacidad;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cboxPasajeros;
        private System.Windows.Forms.DataGridView grdPasajeros;
        private System.Windows.Forms.Button btnAgregar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAgregarEscala;
        private System.Windows.Forms.DataGridView grdEscalas;
        private System.Windows.Forms.ComboBox cboxEscalas;
    }
}