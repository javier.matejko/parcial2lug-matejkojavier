﻿using Motor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial2.Formularios_Vuelos
{
    public partial class Form_Vuelo_AddMod : Form
    {
        Motor.Vuelo gestorVuelo = new Motor.Vuelo();
        Motor.Destino gestorDestino = new Motor.Destino();
        Motor.Ruta gestorRuta = new Motor.Ruta();
        Motor.Tipo gestorTipo = new Motor.Tipo();
        Motor.Pasajero gestorPasajero = new Motor.Pasajero();
        Motor.Vuelo_Pasajeros gestorVuelo_Pasajeros = new Vuelo_Pasajeros();
        Motor.Vuelo_Escalas gestorVuelo_Escalas = new Vuelo_Escalas();
        List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();
        List<Entidades.Pasajero> pasajerosAgregar = new List<Entidades.Pasajero>();
        List<Entidades.Destino> escalas = new List<Entidades.Destino>();
        List<Entidades.Destino> escalasAgregar = new List<Entidades.Destino>();
        public Entidades.Vuelo vuelo;
        public Form_Vuelo_AddMod()
        {
            InitializeComponent();
        }

        private void Form_Vuelo_AddMod_Load(object sender, EventArgs e)
        {
            List<Entidades.Destino> destinos = new List<Entidades.Destino>();
            destinos = gestorDestino.Buscar();
            cboxDestino.DataSource = destinos;
            cboxDestino.DisplayMember = "Descripcion";
            cboxDestino.ValueMember = "Id";
            cboxDestino.SelectedIndex = 0;
                        
            escalas = destinos;
            cboxEscalas.DataSource = escalas;
            cboxEscalas.DisplayMember = "Descripcion";
            cboxEscalas.ValueMember = "Id";
            cboxEscalas.SelectedIndex = 0;

            List<Entidades.Tipo> tipos = new List<Entidades.Tipo>();
            tipos = gestorTipo.Buscar();
            cboxTipo.DataSource = tipos;
            cboxTipo.DisplayMember = "Descripcion";
            cboxTipo.ValueMember = "Id";
            cboxTipo.SelectedIndex = 0;

            List<Entidades.Ruta> rutas = new List<Entidades.Ruta>();
            rutas = gestorRuta.Buscar();
            cboxRuta.DataSource = rutas;
            cboxRuta.DisplayMember = "Descripcion";
            cboxRuta.ValueMember = "Id";
            cboxRuta.SelectedIndex = 0;

            
            pasajeros = gestorPasajero.Buscar();
            cboxPasajeros.DataSource = pasajeros;
            cboxPasajeros.DisplayMember = "Nombre";
            cboxPasajeros.ValueMember = "Id";
            cboxPasajeros.SelectedIndex = 0;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }        

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(pasajerosAgregar.Count > int.Parse(txtCapacidad.Text))
            {
                MessageBox.Show("Overbooking!", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if(vuelo == null)
            {
                vuelo = new Entidades.Vuelo();
                vuelo.Id_Destino = int.Parse(cboxDestino.SelectedValue.ToString());
                vuelo.Id_Tipo = int.Parse(cboxTipo.SelectedValue.ToString());
                vuelo.Id_Ruta = int.Parse(cboxRuta.SelectedValue.ToString());
                vuelo.FechaHora = dtpFecha_Hora.Value;
                vuelo.Capacidad = int.Parse(txtCapacidad.Text);
                vuelo.PorcentajeOcupacion = pasajerosAgregar.Count * 100 / int.Parse(txtCapacidad.Text);
                gestorVuelo.Agregar(vuelo);
                MessageBox.Show("Vuelo agregado con éxito!");

                foreach(Entidades.Pasajero p in pasajerosAgregar)
                {
                    Entidades.Vuelo_Pasajeros vp = new Entidades.Vuelo_Pasajeros();
                    vp.Id_Pasajero = p.Id;
                    gestorVuelo_Pasajeros.Agregar(vp);
                }

                foreach(Entidades.Destino d in escalasAgregar)
                {
                    Entidades.Vuelo_Escalas ve = new Entidades.Vuelo_Escalas();
                    ve.Id_Destino = d.Id;
                    gestorVuelo_Escalas.Agregar(ve);
                }
                
                this.Close();
            }
        }
        private void btnAgregar_Click(object sender, EventArgs e)
        {
            grdPasajeros.DataSource = null;
            pasajerosAgregar.Add(pasajeros[cboxPasajeros.SelectedIndex]);
            pasajeros.RemoveAt(cboxPasajeros.SelectedIndex);
            cboxPasajeros.DataSource = null;
            cboxPasajeros.DataSource = pasajeros;
            cboxPasajeros.DisplayMember = "Nombre";
            cboxPasajeros.ValueMember = "Id";
            cboxPasajeros.SelectedIndex = 0;
            grdPasajeros.DataSource = pasajerosAgregar;
        }
        private void btnAgregarEscala_Click(object sender, EventArgs e)
        {
            grdEscalas.DataSource = null;
            escalasAgregar.Add(escalas[cboxEscalas.SelectedIndex]);
            escalas.RemoveAt(cboxEscalas.SelectedIndex);
            cboxEscalas.DataSource = null;
            cboxEscalas.DataSource = escalas;
            cboxEscalas.DisplayMember = "Descripcion";
            cboxEscalas.ValueMember = "Id";
            cboxEscalas.SelectedIndex = 0;
            grdEscalas.DataSource = escalasAgregar;
        }
    }
}
