﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace Parcial2.Formularios_Vuelos
{
    public partial class Form_Vuelos : Form
    {
        Motor.Vuelo gestorVuelo = new Motor.Vuelo();
        Motor.Pasajero gestorPasajero = new Motor.Pasajero();
        Motor.Vuelo_Escalas gestorVuelo_Escalas = new Motor.Vuelo_Escalas();

        Motor.Destino gestorDestino = new Motor.Destino();
        Motor.Tipo gestorTipo = new Motor.Tipo();
        Motor.Ruta gestorRuta = new Motor.Ruta();

        List<Entidades.Destino> destinos = new List<Entidades.Destino>();

        private int rowSeleccionado = 0;
        private int destinoSeleccionado = 0;
        //private string nombreSeleccionado = "";
        //private int rutaSeleccionada = 0;
        private string coordenadaXSeleccionada = "";
        private string coordenadaYSeleccionada = "";
        private string coordenadaZSeleccionada = "";
        public Form_Vuelos()
        {
            InitializeComponent();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Entidades.Vuelo vuelo = new Entidades.Vuelo();
            vuelo.Id = rowSeleccionado;
            if (gestorVuelo.Cancelar(vuelo) >= 1)
            {
                MessageBox.Show("Se ha cancelado el vuelo con éxito!", "Empleado", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Hubo un error cancelando el vuelo", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            Enlazar();
        }

        private void btnBuscarTodos_Click(object sender, EventArgs e)
        {
            grdDatos.DataSource = null;
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();

            if(chkTodos.Checked)
            {
                vuelos = gestorVuelo.Buscar();
            }
            else if(txtId.Text != "")
            {
                vuelos = gestorVuelo.Buscar(int.Parse(txtId.Text));
            }
            else if(chkPorcentaje.Checked)
            {
                vuelos = gestorVuelo.Buscar(porcentaje:40f);
            }
            grdDatos.DataSource = vuelos;
        }
        private void btnBuscar_Fecha_Click(object sender, EventArgs e)
        {
            grdDatos.DataSource = null;
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();

            vuelos = gestorVuelo.Buscar(dtpFecha_Desde.Value, dtpFecha_Hasta.Value);

            grdDatos.DataSource = vuelos;
        }

        private void btnBuscar_Tipo_Click(object sender, EventArgs e)
        {
            grdDatos.DataSource = null;
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();

            int id_destino = int.Parse(cboxDestino.SelectedValue.ToString());
            int id_tipo = int.Parse(cboxTipo.SelectedValue.ToString());
            int id_ruta = int.Parse(cboxRuta.SelectedValue.ToString());

            vuelos = gestorVuelo.Buscar(id_destino, id_tipo, id_ruta);

            grdDatos.DataSource = vuelos;
        }
        private void Enlazar()
        {
            grdDatos.DataSource = null;
            List<Entidades.Vuelo> vuelos = new List<Entidades.Vuelo>();
            vuelos = gestorVuelo.Buscar();
            grdDatos.DataSource = vuelos;
        }

        private void grdDatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowSeleccionado = int.Parse(grdDatos.Rows[e.RowIndex].Cells[0].Value.ToString());
            destinoSeleccionado = int.Parse(grdDatos.Rows[e.RowIndex].Cells[1].Value.ToString());
            
            grdDatosAdicionales.DataSource = null;
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();
            pasajeros = gestorPasajero.LeerPasajerosEnVuelo(rowSeleccionado);
            grdDatosAdicionales.DataSource = pasajeros;

            grdDatosAdicionalesEscalas.DataSource = null;
            List<Entidades.Destino> escalas = new List<Entidades.Destino>();
            escalas = gestorVuelo_Escalas.Buscar(rowSeleccionado);
            grdDatosAdicionalesEscalas.DataSource = escalas;

            coordenadaXSeleccionada = destinos[destinoSeleccionado].CoordX;
            coordenadaYSeleccionada = destinos[destinoSeleccionado].CoordY; 
            coordenadaZSeleccionada = destinos[destinoSeleccionado].CoordZ;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Form_Vuelo_AddMod form_Vuelo_AddMod = new Form_Vuelo_AddMod();
            form_Vuelo_AddMod.ShowDialog();
            Enlazar();
        }

        private void Form_Vuelos_Load(object sender, EventArgs e)
        {
            
            destinos = gestorDestino.Buscar();
            cboxDestino.DataSource = destinos;
            cboxDestino.DisplayMember = "Descripcion";
            cboxDestino.ValueMember = "Id";
            cboxDestino.SelectedIndex = 0;

            List<Entidades.Tipo> tipos = new List<Entidades.Tipo>();
            tipos = gestorTipo.Buscar();
            cboxTipo.DataSource = tipos;
            cboxTipo.DisplayMember = "Descripcion";
            cboxTipo.ValueMember = "Id";
            cboxTipo.SelectedIndex = 0;

            List<Entidades.Ruta> rutas = new List<Entidades.Ruta>();
            rutas = gestorRuta.Buscar();
            cboxRuta.DataSource = rutas;
            cboxRuta.DisplayMember = "Descripcion";
            cboxRuta.ValueMember = "Id";
            cboxRuta.SelectedIndex = 0;
        }

        private void btnVerMapa_Click(object sender, EventArgs e)
        {
            string link = "https://www.google.com.ar/maps/search/@" + coordenadaXSeleccionada + "," + coordenadaYSeleccionada + "," + coordenadaZSeleccionada + "z/";
            Process.Start(link);
        }

        private void chkTodos_CheckedChanged(object sender, EventArgs e)
        {
            if(chkTodos.Checked)
            {
                txtId.Text = "";
                chkPorcentaje.Checked = false;
            }
        }

        private void chkPorcentaje_CheckedChanged(object sender, EventArgs e)
        {
            if(chkPorcentaje.Checked)
            {
                txtId.Text = "";
                chkTodos.Checked = false;
            }
        }

        private void txtId_TextChanged(object sender, EventArgs e)
        {
            if(txtId.Text != "")
            {
                chkPorcentaje.Checked = false;
                chkTodos.Checked = false;
            }
        }
    }
}
