﻿using Parcial2.Formularios_Destinos;
using Parcial2.Formularios_Pasajeros;
using Parcial2.Formularios_Vuelos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial2
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void verToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //instanciar formulario de vuelos
            Form_Vuelos form_Vuelos = new Form_Vuelos();
            form_Vuelos.ShowDialog();
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //instanciar formulario para agregar vuelos
            Form_Vuelo_AddMod form_Vuelo_AddMod = new Form_Vuelo_AddMod();
            form_Vuelo_AddMod.ShowDialog();
        }

        private void verToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //instanciar formulario de pasajeros
            Form_Pasajeros form_Pasajeros = new Form_Pasajeros();
            form_Pasajeros.ShowDialog();
        }

        private void agregarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //instanciar formulario para agregar pasajeros
            Form_Pasajero_AddMod form_Pasajero_AddMod = new Form_Pasajero_AddMod();
            form_Pasajero_AddMod.ShowDialog();
        }

        private void verToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //instanciar formulario de destinos
            Form_Destinos form_Destinos = new Form_Destinos();
            form_Destinos.ShowDialog();
        }

        private void agregarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            //instanciar formularios para agregar destinos
            Form_Destino_AddMod form_Destino_AddMod = new Form_Destino_AddMod();
            form_Destino_AddMod.ShowDialog();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //salir de la aplicacion
            Application.Exit();
        }
    }
}
