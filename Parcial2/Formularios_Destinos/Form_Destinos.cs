﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial2.Formularios_Destinos
{
    public partial class Form_Destinos : Form
    {
        Motor.Destino gestorDestino = new Motor.Destino();
        private int rowSeleccionado = 0;
        private string nombreSeleccionado = "";
        private int rutaSeleccionada = 0;
        private string coordenadaXSeleccionada = "";
        private string coordenadaYSeleccionada = "";
        private string coordenadaZSeleccionada = "";
        public Form_Destinos()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            grdDatos.DataSource = null;
            List<Entidades.Destino> destinos = new List<Entidades.Destino>();
            if (chkTodos.Checked || txtId.Text == "" && txtDescripcion.Text == "")
            {
                destinos = gestorDestino.Buscar();
            }
            else if (txtDescripcion.Text != "")
            {
                destinos = gestorDestino.Buscar(txtDescripcion.Text);
            }
            else
            {
                destinos = gestorDestino.Buscar(int.Parse(txtId.Text));
            }
            grdDatos.DataSource = destinos;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Entidades.Destino destino = new Entidades.Destino();
            destino.Id = rowSeleccionado;
            destino.Descripcion = nombreSeleccionado;
            destino.Id_Ruta = rutaSeleccionada;
            destino.CoordX = coordenadaXSeleccionada;
            destino.CoordY = coordenadaYSeleccionada;
            destino.CoordZ = coordenadaZSeleccionada;
            Form_Destino_AddMod frm_Destino_AddMod = new Form_Destino_AddMod();
            frm_Destino_AddMod.destino = destino;
            frm_Destino_AddMod.ShowDialog();
            Enlazar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Entidades.Destino destino = new Entidades.Destino();
            destino.Id = rowSeleccionado;
            if (gestorDestino.Eliminar(destino) >= 1)
            {
                MessageBox.Show("Se ha eliminado el destino con éxito!", "Empleado", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("Hubo un error eliminando el destino", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            Enlazar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Form_Destino_AddMod form_Destino_AddMod = new Form_Destino_AddMod();
            form_Destino_AddMod.ShowDialog();
            Enlazar();
        }

        private void grdDatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowSeleccionado = int.Parse(grdDatos.Rows[e.RowIndex].Cells[0].Value.ToString());
            rutaSeleccionada = int.Parse(grdDatos.Rows[e.RowIndex].Cells[1].Value.ToString());
            nombreSeleccionado = grdDatos.Rows[e.RowIndex].Cells[2].Value.ToString();
            coordenadaXSeleccionada = grdDatos.Rows[e.RowIndex].Cells[3].Value.ToString();
            coordenadaYSeleccionada = grdDatos.Rows[e.RowIndex].Cells[4].Value.ToString();
            coordenadaZSeleccionada = grdDatos.Rows[e.RowIndex].Cells[5].Value.ToString();
        }

        private void Enlazar()
        {
            grdDatos.DataSource = null;
            List<Entidades.Destino> destinos = new List<Entidades.Destino>();
            destinos = gestorDestino.Buscar();
            grdDatos.DataSource = destinos;
        }
    }
}
