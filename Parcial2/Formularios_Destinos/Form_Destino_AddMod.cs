﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Parcial2.Formularios_Destinos
{
    public partial class Form_Destino_AddMod : Form
    {
        Motor.Destino gestorDestino = new Motor.Destino();
        Motor.Ruta gestorRuta = new Motor.Ruta();
        public Entidades.Destino destino;
        public Form_Destino_AddMod()
        {
            InitializeComponent();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (txtDescripcion.Text != "" && txtCoordX.Text != "" && txtCoordY.Text != "" && txtCoordZ.Text != "")
            {
                if (destino == null) //si es un nuevo destino
                {
                    destino = new Entidades.Destino();
                    destino.Descripcion = txtDescripcion.Text;
                    destino.Id_Ruta = int.Parse(cboxRuta.SelectedValue.ToString());
                    destino.CoordX = txtCoordX.Text;
                    destino.CoordY = txtCoordY.Text;
                    destino.CoordZ = txtCoordZ.Text;
                    gestorDestino.Agregar(destino);
                    MessageBox.Show("Se agregó el destino correctamente!");
                    this.Close();
                }
                else if (destino != null) //si es una modificacion de destino
                {
                    destino.Id_Ruta = int.Parse(cboxRuta.SelectedValue.ToString());
                    destino.Descripcion = txtDescripcion.Text;
                    destino.CoordX = txtCoordX.Text;
                    destino.CoordY = txtCoordY.Text;
                    destino.CoordZ = txtCoordZ.Text;
                    gestorDestino.Guardar(destino);
                    MessageBox.Show("Se modificó el destino correctamente!");
                    this.Close();
                }
            }
            else
            {
                MessageBox.Show("Error al cargar el destino...");
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtDescripcion.Text = "";
            txtCoordX.Text = txtCoordY.Text = txtCoordZ.Text = "";
            cboxRuta.SelectedIndex = 0;
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form_Destino_AddMod_Load(object sender, EventArgs e)
        {
            List<Entidades.Ruta> rutas = new List<Entidades.Ruta>();
            rutas = gestorRuta.Buscar();
            cboxRuta.DataSource = rutas;
            cboxRuta.DisplayMember = "Descripcion";
            cboxRuta.ValueMember = "Id";
            cboxRuta.SelectedIndex = 0;

            if (destino != null)
            {
                txtDescripcion.Text = destino.Descripcion;
                txtCoordX.Text = destino.CoordX.ToString();
                txtCoordY.Text = destino.CoordY.ToString();
                txtCoordZ.Text = destino.CoordZ.ToString();
                cboxRuta.SelectedValue = destino.Id_Ruta;
            }
        }
    }
}
