﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Motor;
using Entidades;

namespace Parcial2.Formularios_Pasajeros
{
    public partial class Form_Pasajeros : Form
    {
        Motor.Pasajero gestorPasajero = new Motor.Pasajero();
        private int rowSeleccionado = 0;
        private string nombreSeleccionado = "";
        public Form_Pasajeros()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            grdDatos.DataSource = null;
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();
            if (chkTodos.Checked || txtId.Text == "" && txtNombre.Text == "")
            {
                pasajeros = gestorPasajero.Buscar();
            }
            else if (txtNombre.Text != "")
            {
                pasajeros = gestorPasajero.Buscar(txtNombre.Text);
            }
            else
            {
                pasajeros = gestorPasajero.Buscar(int.Parse(txtId.Text));
            }
            grdDatos.DataSource = pasajeros;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Entidades.Pasajero pasajero = new Entidades.Pasajero();
            pasajero.Id = rowSeleccionado;
            pasajero.Nombre = nombreSeleccionado;
            Form_Pasajero_AddMod frmPasajeros_AddMod = new Form_Pasajero_AddMod();
            frmPasajeros_AddMod.pasajero = pasajero;
            frmPasajeros_AddMod.ShowDialog();
            Enlazar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Entidades.Pasajero pasajero = new Entidades.Pasajero();
            pasajero.Id = rowSeleccionado;
            if (gestorPasajero.Eliminar(pasajero) >= 1)
            {
                MessageBox.Show("Se ha eliminado al pasajero con éxito!", "Empleado", MessageBoxButtons.OK);
            }            
            else
            {
                MessageBox.Show("Hubo un error eliminando al pasajero", "Atención!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            Enlazar();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            Form_Pasajero_AddMod frmPasajeros_AddMod = new Form_Pasajero_AddMod();
            frmPasajeros_AddMod.ShowDialog();
            Enlazar();
        }

        private void grdDatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            rowSeleccionado = int.Parse(grdDatos.Rows[e.RowIndex].Cells[0].Value.ToString());
            nombreSeleccionado = grdDatos.Rows[e.RowIndex].Cells[1].Value.ToString();
        }

        private void Enlazar()
        {
            grdDatos.DataSource = null;
            List<Entidades.Pasajero> pasajeros = new List<Entidades.Pasajero>();
            pasajeros = gestorPasajero.Buscar();
            grdDatos.DataSource = pasajeros;
        }
    }
}
