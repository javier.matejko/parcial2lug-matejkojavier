﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Motor;
using Entidades;

namespace Parcial2.Formularios_Pasajeros
{
    public partial class Form_Pasajero_AddMod : Form
    {
        Motor.Pasajero gestorPasajero = new Motor.Pasajero();        
        public Entidades.Pasajero pasajero;
        public Form_Pasajero_AddMod()
        {
            InitializeComponent();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtNombre.Text = "";
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(txtNombre.Text != "")
            {
                if (pasajero == null)
                {
                    pasajero = new Entidades.Pasajero();
                    pasajero.Nombre = txtNombre.Text;
                    gestorPasajero.Agregar(pasajero);
                    MessageBox.Show("Se agregó el pasajero correctamente!");
                    this.Close();
                }
                else if (pasajero != null)
                {
                    pasajero.Nombre = txtNombre.Text;
                    gestorPasajero.Guardar(pasajero);
                    MessageBox.Show("Se modificó el pasajero correctamente!");
                    this.Close();
                }
            }            
            else
            {
                MessageBox.Show("Error al cargar el pasajero...");
            }
            
        }

        private void Form_Pasajero_AddMod_Load(object sender, EventArgs e)
        {
            if(pasajero != null)
            {
                txtNombre.Text = pasajero.Nombre;
            }            
        }
    }
}
